'use strict';

/**
 * @ngdoc function
 * @name synergistProjectApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the synergistProjectApp
 */
angular.module('synergistProjectApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

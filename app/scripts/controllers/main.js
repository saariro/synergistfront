'use strict';

/**
 * @ngdoc function
 * @name synergistProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the synergistProjectApp
 */
angular.module('synergistProjectApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
